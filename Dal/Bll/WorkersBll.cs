﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bll
{
    public class WorkersBll
    {
        //שליפת כל נתוני העובדים ללא שימוש במחלקות EF
        public static List<Dto.WorkersDto> GetAllWorkers()
        {
            return ConvertersEF.WorkersConvert.ToDtoList(Dal.WorkersDal.SelectAllWorkers());
        }
        public static void AddNewWorker(Dto.WorkersDto w)
        {
            Dal.WorkersDal.PutNewWorker(ConvertersEF.WorkersConvert.ToDal(w));
        }
        public static bool PostWorker(Dto.WorkersDto w)
        {
            return Dal.WorkersDal.PostWorker(ConvertersEF.WorkersConvert.ToDal(w));
        }
        public static bool DeleteWorker(Dto.WorkersDto w)
        {
            return Dal.WorkersDal.DeleteWorker(ConvertersEF.WorkersConvert.ToDal(w));
        }
    }
}
