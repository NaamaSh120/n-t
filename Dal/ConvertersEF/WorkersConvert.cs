﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertersEF
{
    public class WorkersConvert
    {
        //ממירה מחלקה של מיקרוסופט לשלנו
        public static Dto.WorkersDto ToDto(Dal.Workers w)
        {
            Dto.WorkersDto wo = new Dto.WorkersDto(w.code, w.ID, w.name, w.sisma, w.adress, w.phon, w.mail, w.pay_for_hour, w.code_role);
            return wo;
        }
        //ממירה מחלקה שלנו לשל מיקרוסופט
        public static Dal.Workers ToDal(Dto.WorkersDto w)
        {
            Dal.Workers wo = new Dal.Workers();
            wo.code = w.Code;
            wo.ID = w.ID;
            wo.name = w.Name;
            wo.sisma = w.Sisma;
            wo.adress = w.Adress;
            wo.phon = w.Phon;
            wo.mail = w.Mail;
            wo.pay_for_hour = w.Pay_for_hour;
            wo.code_role = w.Code_role;
            return wo;
        }
        //ממירה אוסף של מירוסופט לאוסף שלנו
        public static List<Dal.Workers> ToDalList(List<Dto.WorkersDto> lw)
        {
            List<Dal.Workers> lwn = new List<Dal.Workers>();
            foreach (Dto.WorkersDto w in lw)
            {
                lwn.Add(ToDal(w));
            }
            return lwn;
        }
        //ממירה אוסף שלנו לאוסף של מירוסופט 
        public static List<Dto.WorkersDto> ToDtoList(List<Dal.Workers> lw)
        {
            List<Dto.WorkersDto> lwn = new List<Dto.WorkersDto>();
            foreach (Dal.Workers w in lw)
            {
                lwn.Add(ToDto(w));
            }
            return lwn;
        }
    }
}
