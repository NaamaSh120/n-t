﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class WorkersDal
    {
        private static BakeryEntities db = DBobject.GetDB();
        public static List<Workers> SelectAllWorkers()
        {
            using (db)
            {
                return db.Workers.ToList();
            }
        }
        public static void PutNewWorker(Workers w)
        {
            using(db)
            {
                db.Workers.Add(w);
            }
        }
        public static bool PostWorker(Workers w)
        {
            using (db)
            {
                var f = db.Workers.FirstOrDefault(wr => wr.ID == w.ID);
                if(f!=null)
                {
                    f = w;
                    return true;
                }
                return false;
            }
        }
        public static bool DeleteWorker(Workers w)
        {
            using(db)
            {
                var f = db.Workers.FirstOrDefault(wr => wr.ID == w.ID);
                if(f!=null)
                {
                    db.Workers.Remove(f);
                    return true;
                }
                return false;
            }
        }
    }
}
