﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class WorkersDto
    {
        public int Code { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string Sisma { get; set; }
        public string Adress { get; set; }
        public string Phon { get; set; }
        public string Mail { get; set; }
        public decimal Pay_for_hour { get; set; }
        public int Code_role { get; set; }

        public WorkersDto()
        {

        }
        public WorkersDto(int code, string id, string name, string sisma, string adress, string phon, string mail, decimal pay_for_hour, int code_role)
        {
            Code = code;
            ID = id;
            Name = name;
            Sisma = sisma;
            Adress = adress;
            Phon = phon;
            Mail = mail;
            Pay_for_hour = pay_for_hour;
            Code_role = code_role;
        }
    }
}
