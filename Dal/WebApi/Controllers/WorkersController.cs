﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class WorkersController : ApiController
    {
        public IHttpActionResult GetWorkers()
        {
            try
            {
                var q = Bll.WorkersBll.GetAllWorkers();
                if (q != null)
                    return Ok(q);
                return NotFound();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        public IHttpActionResult PutWorker(Dto.WorkersDto worker)
        {
            try
            {
                Bll.WorkersBll.AddNewWorker(worker);
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        public IHttpActionResult PostWorker(Dto.WorkersDto worker)
        {
            try
            {
                bool ans=Bll.WorkersBll.PostWorker(worker);
                if(ans==false)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        public IHttpActionResult DeleteWorker(Dto.WorkersDto worker)
        {
            try
            {
                bool ans = Bll.WorkersBll.DeleteWorker(worker);
                if (ans == false)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
