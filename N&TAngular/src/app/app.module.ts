import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { LoginForWorkersComponent } from './login-for-workers/login-for-workers.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WorkersHomePageComponent } from './workers-home-page/workers-home-page.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';
import { OrderDetailsForSenderComponent } from './order-details-for-sender/order-details-for-sender.component';
import { OrderDetailsForBakerComponent } from './order-details-for-baker/order-details-for-baker.component';
import { WatchRecipeComponent } from './watch-recipe/watch-recipe.component';
// import { HomePageComponent } from './home-page/home-page.component';

@NgModule({
  declarations: [
    AppComponent,
    // LoginForWorkersComponent,
    WorkersHomePageComponent,
    OrderDetailsForSenderComponent,
    OrderDetailsForBakerComponent,
    WatchRecipeComponent
    // HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSliderModule,
    MatTableModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
