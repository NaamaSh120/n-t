export class GeneralOrder {
    code:number;
    consumerCode:number;
    consumerName:String;
    consumerPhone:String;
    supplyAdress:String;
    status:orderStatus;
    supplyDate:Date;
    workerCode:number;
    totalPrice:number;
    //I should put here - and not in the c# object - a variable to know if the supply is available or not
    //isAvailableToWorkOn:boolean;
    //or not? I can caculate this from the date
    constructor(code:number,consumerCode:number,consumerName:string,consumerPhone:string,supplyAdress:string,supplyDate:Date,totalPrice:number){
        this.code=code;
        this.consumerCode=consumerCode;
        this.consumerName=consumerName;
        this.consumerPhone=consumerPhone;
        this.supplyAdress=supplyAdress;
        this.status=orderStatus.NotTouched;
        this.supplyDate=supplyDate;
        this.workerCode=null;
        this.totalPrice=totalPrice;
    }
    
}
export enum orderStatus{
     NotTouched=1,
     Ready=2,
     InProcess=4,
     wasSupplied=8   
}
