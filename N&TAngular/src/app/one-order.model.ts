import { orderStatus } from './generalOrder.model';

export class OneOrder {
    code:number;
    //??generalOrderCode:number;
    productCode:number;
    //should I put it here, to prevent another call to the server?
    productName:string;
    productAmount:number;
    //??bakerCode:number;
    status:orderStatus;
}
