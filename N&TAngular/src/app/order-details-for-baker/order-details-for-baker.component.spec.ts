import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsForBakerComponent } from './order-details-for-baker.component';

describe('OrderDetailsForBakerComponent', () => {
  let component: OrderDetailsForBakerComponent;
  let fixture: ComponentFixture<OrderDetailsForBakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsForBakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsForBakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
