import { Component, OnInit } from '@angular/core';
import {OneOrder} from "src/app/one-order.model";
import {orderStatus} from 'src/app/generalOrder.model';

@Component({
  selector: 'app-order-details-for-baker',
  templateUrl: './order-details-for-baker.component.html',
  styleUrls: ['./order-details-for-baker.component.css']
})
export class OrderDetailsForBakerComponent implements OnInit {
  public basket:OneOrder[]=[
    {code:0,productCode:234,productName:"ghghg",productAmount:10,status:orderStatus.NotTouched},
    {code:1,productCode:28,productName:"klklk",productAmount:20,status:orderStatus.NotTouched},
    {code:2,productCode:4,productName:"opopo",productAmount:4,status:orderStatus.NotTouched}
  ];
  public columnsToDisplay=["productName","productAmount","status","timeCalculation","watchRecipeBtn"];
  public WorkerInCode:number;
  public orderInCode:number;
  constructor() {

   }

  ngOnInit(): void {
  }

}
