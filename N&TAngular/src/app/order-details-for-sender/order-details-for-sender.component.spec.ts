import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsForSenderComponent } from './order-details-for-sender.component';

describe('OrderDetailsForSenderComponent', () => {
  let component: OrderDetailsForSenderComponent;
  let fixture: ComponentFixture<OrderDetailsForSenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsForSenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsForSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
