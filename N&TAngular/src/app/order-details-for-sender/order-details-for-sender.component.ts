import { Component, OnInit } from '@angular/core';
import {orderStatus} from 'src/app/generalOrder.model';
import {GeneralOrder} from 'src/app/generalOrder.model';

@Component({
  selector: 'app-order-details-for-sender',
  templateUrl: './order-details-for-sender.component.html',
  styleUrls: ['./order-details-for-sender.component.css']
})
export class OrderDetailsForSenderComponent implements OnInit { 
  thisOrder:GeneralOrder;
  workerInCode:number;
  constructor() {
    //code:number,consumerName:String,consumerPhone:String,supplyAdress:String,supplyDate:Date,workerInCode:number
    this.thisOrder.code=0;//=code;
    this.thisOrder.consumerName="hjj";//=consumerName;
    this.thisOrder.consumerPhone="9876";//=consumerPhone;
    this.thisOrder.supplyAdress="ghjkj2 htgh";//=supplyAdress;
    this.thisOrder.supplyDate=new Date();//=supplyDate;
    this.workerInCode=0;//=workerInCode;
   }

  ngOnInit(): void {
  }

}
