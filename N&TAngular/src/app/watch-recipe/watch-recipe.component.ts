import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-watch-recipe',
  templateUrl: './watch-recipe.component.html',
  styleUrls: ['./watch-recipe.component.css']
})
export class WatchRecipeComponent implements OnInit {
  public productCode:number;
  constructor() { 
    this.writeRecipe();
  }
  writeRecipe():void{
    //fill the paragraph in the html page according to server call
  }
  ngOnInit(): void {
  }

}
