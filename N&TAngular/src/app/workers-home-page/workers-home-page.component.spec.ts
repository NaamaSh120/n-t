import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersHomePageComponent } from './workers-home-page.component';

describe('WorkersHomePageComponent', () => {
  let component: WorkersHomePageComponent;
  let fixture: ComponentFixture<WorkersHomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkersHomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkersHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
