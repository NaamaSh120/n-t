import { createInjectable } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import {GeneralOrder} from 'src/app/generalOrder.model';
import {orderStatus} from 'src/app/generalOrder.model';
@Component({
  selector: 'app-workers-home-page',
  templateUrl: './workers-home-page.component.html',
  styleUrls: ['./workers-home-page.component.css']
})
export class WorkersHomePageComponent implements OnInit {
  public orders:GeneralOrder[]=[
    {code:1,consumerCode:null,consumerName:"asdf",consumerPhone:"0988768",supplyAdress:"g2 street",supplyDate:new Date("2021-01-01"),status:orderStatus.NotTouched,workerCode:0,totalPrice:200},
    {code:2,consumerCode:null,consumerName:"asdhjk",consumerPhone:"098909868",supplyAdress:"g3 street",supplyDate:new Date("2021-01-01"),status:orderStatus.NotTouched,workerCode:0,totalPrice:260},
    {code:3,consumerCode:4,consumerName:null,consumerPhone:null,supplyAdress:"s67 street",supplyDate:new Date("2021-01-01"),status:orderStatus.NotTouched,workerCode:0,totalPrice:267},
  ];
  public columnsToDisplay=["code","supplyDate","status"];
  workerInCode:number;
  constructor() {//workerInCode:number
    this.workerInCode=0;//=workerInCode
    this.createTable();
   }
   createTable():void{
    //creating the table according to the type of worker - sender or baker
   }
  ngOnInit(): void {
  }

}
